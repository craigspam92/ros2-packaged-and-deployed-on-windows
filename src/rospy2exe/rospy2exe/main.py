import rclpy
from rclpy.node import Node, Publisher
from std_msgs.msg import String

def main():
    print("initializing rclpy")
    rclpy.init()
    print("creating node")
    node = Node("test_node")
    print("creating publisher")
    pub: Publisher = node.create_publisher(String, "test_topic", 1)

    def timer_callback():
        print("creating message")
        msg = String(data="Hello World!")
        print("publishing message")
        pub.publish(msg)
    print("creating timer")
    timer = node.create_timer(1.0, timer_callback)

    print("spinning node")
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        print("KeyboardInterrupt")
    
    print("goodbye")

if __name__ == '__main__':
    main()
